package spring.demo.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDto {
  private Integer id;
  private String email;
  private String password;
  private String name;
  private String surname;
  private List<ContactDto> contacts;
}
