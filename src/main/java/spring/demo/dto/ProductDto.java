package spring.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Integer id;
    private String category_id;
    private String icon_base64;
    private String name_product;
    private String brand_product;
    private String description;
    private String price;
    private Integer count;
    private String time;
}
