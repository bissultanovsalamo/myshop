package spring.demo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.stereotype.Component;

@Component
public class Utils {

    String url = "jdbc:postgresql://localhost:5433/postgres";
    String user = "postgres";
    String password = "1";


    public Connection getConnection() {
      try {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(url, user, password);
      } catch (SQLException | ClassNotFoundException e) {
        e.printStackTrace();
      }
      return null;
    }

    public static Integer toInt(String str){
      try {
        Integer.parseInt(str);
      }catch (Exception e){
        e.printStackTrace();
      }
      return null;
    }

  }
