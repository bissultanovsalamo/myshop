package spring.demo.controllers;

import java.sql.SQLException;
import java.text.AttributedString;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import spring.demo.dto.CategoryProductDto;
import spring.demo.dto.ProductDto;
import spring.demo.dto.UserDto;
import spring.demo.services.PersonService;

@Controller
@RequestMapping("/person")
public class PersonController {
    private static UserDto userDto = new UserDto();
    private static List<ProductDto> productDto = new ArrayList<>();
    private static int price = 0;
    private static  int count = 1;


    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView passParametersWithModelAndView(@RequestParam(value = "username", required = false) String username,
                                                       @RequestParam(value = "password", required = false) String password) {
       price = 0;
        if (!username.equals("")) {
            UserDto loginDto = new UserDto();
            loginDto.setEmail(username);
            loginDto.setPassword(password);
            userDto = personService.checkLogin(loginDto);

            List<CategoryProductDto> productDto = personService.getHome();
            if (Objects.isNull(userDto)) {
                ModelAndView modelAndView = new ModelAndView("index");
                modelAndView.addObject("result", "Не удаётся войти.\n" +
                        "Пожалуйста, проверьте правильность написания логина и пароля.");
                return modelAndView;
            } else {
                ModelAndView modelAndView = new ModelAndView("home");
                for (CategoryProductDto category : productDto) {
                    modelAndView.addObject("result", "Вы вошли личный кабинет");
                    modelAndView.addObject("person_id", userDto.getId());
                    modelAndView.addObject("name", userDto.getName());
                    modelAndView.addObject("surname", userDto.getSurname());
                    modelAndView.addObject("id", category.getId());
                    modelAndView.addObject("name_category", category.getName_category());
                    modelAndView.addObject("icon_base64", category.getIcon_base64());
                    modelAndView.addObject("array", productDto);
                }
                return modelAndView;
            }
        }
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("result", "Вы не велли данные повторите ввод");
        return modelAndView;
    }

    @RequestMapping(value = "/result", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView getResultPage(@RequestParam(value = "id", required = false) Integer id) {

        List<ProductDto> productDto = personService.getPersons(id);

        ModelAndView modelAndView = new ModelAndView("phone");
        modelAndView.addObject("name", userDto.getName());
        modelAndView.addObject("surname", userDto.getSurname());
        modelAndView.addObject("array", productDto);
        return modelAndView;


    }


    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView passParametersWithModelAndVie(@RequestParam(value = "login", required = false) String username,
                                                      @RequestParam(value = "password", required = false) String password,
                                                      @RequestParam(value = "name", required = false) String name,
                                                      @RequestParam(value = "surname", required = false) String surname) throws Exception {
        UserDto loginDto = new UserDto();
        loginDto.setEmail(username);
        loginDto.setPassword(password);
        loginDto.setName(name);
        loginDto.setSurname(surname);

        String str = personService.add(loginDto);
        if (str.equals("Ошибка регистрации!")) {
            ModelAndView modelAndView = new ModelAndView("registration");
            modelAndView.addObject("result", "Ошибка регистрации!");
            return modelAndView;
        } else {

            ModelAndView modelAndView = new ModelAndView("index");
            modelAndView.addObject("result", "Регистрация прошла успешна!");
            return modelAndView;
        }


    }


    @RequestMapping(value = "/phone", method = RequestMethod.GET)

    public ModelAndView getPersons(@RequestParam(value = "id", required = false) Integer id) {

        ProductDto p = personService.getInfo(id);
        if(p.getCount() == 0){
            ModelAndView modelAndView = new ModelAndView("result");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            modelAndView.addObject("id", p.getId());
            modelAndView.addObject("icon_base64", p.getIcon_base64());
            modelAndView.addObject("name_product", p.getName_product());
            modelAndView.addObject("price", p.getPrice());
            modelAndView.addObject("count", p.getCount());
            modelAndView.addObject("price", "Товара нет вналичи");
            return modelAndView;

        }else {
            ModelAndView modelAndView = new ModelAndView("result");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            modelAndView.addObject("id", p.getId());
            modelAndView.addObject("icon_base64", p.getIcon_base64());
            modelAndView.addObject("name_product", p.getName_product());
            modelAndView.addObject("brand_product", p.getBrand_product());
            modelAndView.addObject("description", p.getDescription());
            modelAndView.addObject("price", p.getPrice());
            modelAndView.addObject("count", p.getCount());
            return modelAndView;
        }
    }


    @RequestMapping(value = "/korzina", method = RequestMethod.GET)
    public ModelAndView createPerson(@RequestParam(value = "id", required = false) Integer id) {
        price = 0;
        Integer person_id = userDto.getId();
        personService.getKor(id, person_id);
        List<CategoryProductDto> productDto = personService.getHome();

        ModelAndView modelAndView = new ModelAndView("home");
        modelAndView.addObject("result", "Вы вошли личный кабинет");
        modelAndView.addObject("name", userDto.getName());
        modelAndView.addObject("surname", userDto.getSurname());
        modelAndView.addObject("array", productDto);
        return modelAndView;

    }

    @RequestMapping(value = "/gerKorzina", method = RequestMethod.GET)
    public ModelAndView updatePerson() {
       count = 1;
        Integer id = userDto.getId();
       productDto = personService.getKorzin(id);
       if (productDto.size() == 0) {
            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("error", "Корзина пуста!");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            return modelAndView;
        } else {
            int a = 0;
            for (ProductDto p : productDto) {
                price += a + Integer.parseInt(p.getPrice());


            }

            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            modelAndView.addObject("array", productDto);
            modelAndView.addObject("prince", price);
            modelAndView.addObject("plus",count);
            modelAndView.addObject("str", "Общая сумма товара:$");
            modelAndView.addObject("button", "Оплатить");
            return modelAndView;
        }

    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)

    public ModelAndView deletePersonById1() {
        List<CategoryProductDto> productDto = personService.getHome();
        ModelAndView modelAndView = new ModelAndView("home");
        modelAndView.addObject("result", "Вы вошли личный кабинет");
        modelAndView.addObject("name", userDto.getName());
        modelAndView.addObject("surname", userDto.getSurname());
        modelAndView.addObject("array", productDto);
        return modelAndView;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView updatePerson1(@RequestParam(value = "id", required = false) Integer id) throws SQLException {
       count = 1;
        personService.getDelete(id);
       userDto.getId();
       productDto = personService.getKorzin(userDto.getId());

        if (productDto.size() == 0) {
            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("error", "Корзина пуста!");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            return modelAndView;
        } else {
          price = 0;
            for (ProductDto p : productDto) {

                price += Integer.parseInt(p.getPrice());


            }
            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            modelAndView.addObject("array", productDto);
            modelAndView.addObject("prince", price);
            modelAndView.addObject("str", "Общая сумма товара:$");
            modelAndView.addObject("button", "Оплатить");
            return modelAndView;
        }


    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public ModelAndView createPerson1() {
        System.out.println("Hello world");

        Integer person_id = userDto.getId();
        Integer cash = personService.getCash(person_id);



        ModelAndView modelAndView = new ModelAndView("productInfo");
        modelAndView.addObject("id", userDto.getId());
        modelAndView.addObject("name", userDto.getName());
        modelAndView.addObject("surname", userDto.getSurname());
        modelAndView.addObject("price", price);
        modelAndView.addObject("cash", cash);
        return modelAndView;

    }

    @RequestMapping(value = "/money", method = RequestMethod.GET)
    public ModelAndView createPerson11() {
        Integer cash = personService.getCash(userDto.getId());
        Integer a = cash - price;


        count = 1;

        String str = personService.setolata(userDto.getId(), a);
        System.out.println(str);
        if (str.equals("Спасибо за покупку")) {
            ModelAndView modelAndView = new ModelAndView("finesh");
            modelAndView.addObject("result", "Спасибо за покупку");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("productInfo");
            modelAndView.addObject("result", str);
            modelAndView.addObject("id", userDto.getId());
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            modelAndView.addObject("price", price);
            modelAndView.addObject("cash", cash);
            return modelAndView;


        }



    }
    @RequestMapping(value = "/moneyt", method = RequestMethod.GET)
    public ModelAndView createPerson111 (@RequestParam(value = "id", required = false) Integer id) {
        personService.getKor(id, userDto.getId());
        List<ProductDto> productDtos = personService.getKorzin(userDto.getId());
        price = 0;
        if (productDto.size() == 0) {
            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("error", "Корзина пуста!");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            return modelAndView;
        } else {
            for (ProductDto p:productDto) {
                if(id == p.getId()){
                    for (ProductDto ps:productDtos) {
                        if(id==ps.getId()){
                            int a = Integer.parseInt(p.getPrice());
                            a+= Integer.parseInt(ps.getPrice());
                            p.setPrice(String.valueOf(a));
                            count++;
                            personService.productCount(id,count);
                        }
                    }
                }
                price += Integer.parseInt(p.getPrice());

            }


            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            modelAndView.addObject("array", productDto);
            modelAndView.addObject("prince", price);
            modelAndView.addObject("plus",count);
            modelAndView.addObject("str", "Общая сумма товара:$");
            modelAndView.addObject("button", "Оплатить");
            return modelAndView;
        }
    }
    @RequestMapping(value = "/moneys", method = RequestMethod.GET)
    public ModelAndView createPerson1112 (@RequestParam(value = "id", required = false) Integer id) throws SQLException {
        personService.getDelete(id);

        List<ProductDto> productDtos = personService.getKorzin(userDto.getId());


        if (productDtos.size() == 0) {
            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("error", "Корзина пуста!");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            return modelAndView;
        } else {
            for (ProductDto p:productDto) {
                if(id == p.getId()){
                    for (ProductDto ps:productDtos) {
                        if(id==ps.getId()){
                            int a = Integer.parseInt(p.getPrice());
                            a-= Integer.parseInt(ps.getPrice());
                            p.setPrice(String.valueOf(a));
                            count--;
                            price -= Integer.parseInt(ps.getPrice());
                           personService.productId(id, count);
                        }

                    }
                }

            }


            ModelAndView modelAndView = new ModelAndView("korzina");
            modelAndView.addObject("result", "Вы вошли личный кабинет");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            modelAndView.addObject("array", productDto);
            modelAndView.addObject("prince", price);
            modelAndView.addObject("plus",count);

            modelAndView.addObject("str", "Общая сумма товара:$");
            modelAndView.addObject("button", "Оплатить");
            return modelAndView;
        }
    }
}