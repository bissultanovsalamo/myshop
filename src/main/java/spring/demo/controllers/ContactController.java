package spring.demo.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import spring.demo.dto.ContactDto;
import spring.demo.services.ContactService;
import spring.demo.services.PersonService;

@Controller
@RequestMapping("/contact")
public class ContactController {

  @Autowired
  private PersonService personService;

  @Autowired
  private ContactService contactService;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  private void getContacts() {
    System.out.println("get all Contacts");
  }

  @RequestMapping(value = "/id", method = RequestMethod.GET)
  @ResponseBody
  private ContactDto getContactById(@RequestParam(value = "id", required = false) int id) {
    System.out.println("get contact by id  = " + id);
    return null;
  }

  @RequestMapping(value = "person/id", method = RequestMethod.GET)
  @ResponseBody
  private List<ContactDto> getContactsByPersonId(@RequestParam(value = "person_id", required = false) Integer personId) {
    return contactService.getContactsByPersonId(personId);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  private void createContact(@RequestParam(value = "tel", required = false) String tel,
                             @RequestParam(value = "mail", required = false) String mail,
                             @RequestParam(value = "person_id", required = false) String personId) {
    System.out.println("create contact by tel = " + tel + "  mail = " + mail);
  }

  @RequestMapping(value = "/", method = RequestMethod.PUT)
  private void updateContact(@RequestParam(value = "id", required = false) Integer id,
                             @RequestParam(value = "tel", required = false) String tel,
                             @RequestParam(value = "mail", required = false) String mail,
                             @RequestParam(value = "person_id", required = false) String personId) {
    System.out.println("ID = " + id);
    System.out.println("update contact by tel = " + tel + "  mail = " + mail);
  }

  @RequestMapping(value = "/", method = RequestMethod.DELETE)
  private void deleteContactById(@RequestParam(value = "id", required = false) @PathVariable Integer id) {
    System.out.println("delete contact by id  = " + id);
  }

}
