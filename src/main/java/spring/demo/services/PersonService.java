package spring.demo.services;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.CategoryProductDto;
import spring.demo.dto.ProductDto;
import spring.demo.dto.UserDto;
import spring.demo.repository.ContactRepository;
import spring.demo.repository.PersonRepository;

@Service
public class PersonService {


  @Autowired
  private PersonRepository personRepository;

  @Autowired
  private ContactRepository contactRepository;

  public UserDto checkLogin(UserDto loginDto) {

  return personRepository.checkLogin(loginDto);


  }

  public List<ProductDto> getPersons(Integer id) {
    List<ProductDto> list = personRepository.getPersons(id);

//    for (UserDto item : list) {
//      item.setContacts(contactRepository.getContactsByPersonId(item.getId()));
//    }

    return list;
  }


  public String add(UserDto userDto) throws Exception {
   String str = personRepository.add(userDto);
   return str;
  }

  public List<CategoryProductDto> getHome() {
    List<CategoryProductDto> productDtos = personRepository.getHome();
    return productDtos;
  }

    public ProductDto getInfo(Integer id) {
    ProductDto productDto = personRepository.getInfo(id);
    return productDto;
    }

  public void getKor(Integer id, Integer idi ) {
    personRepository.setAddKor(id, idi);

  }



  public List<ProductDto> getKorzin(Integer id) {
   List<ProductDto> list =  personRepository.getKorzin(id);

   return list;
  }

  public String getDelete( Integer id) throws SQLException {
      personRepository.delete(id);

return "Удаление прошло успешно";

  }

  public void addPrice() {
    personRepository.addPrice();
  }


    public String setolata(Integer id, int price) {
   String str = personRepository.setOplata(id,price);
   return str;
    }

  public Integer getCash(Integer id) {
   Integer cash =  personRepository.getCash(id);
   return cash;
  }

    public void productCount(Integer id, int count) {
      personRepository.productCount(id, count);
    }

    public void productId(Integer id, int count) {
      personRepository.productId(id, count);
    }
}
