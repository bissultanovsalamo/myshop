package spring.demo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.ContactDto;
import spring.demo.repository.ContactRepository;

@Service
public class ContactService {

  @Autowired
  private ContactRepository contactRepository;

  public List<ContactDto> getContactsByPersonId(Integer personId){
    return contactRepository.getContactsByPersonId(personId);
  }

}
