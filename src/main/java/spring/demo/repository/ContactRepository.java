package spring.demo.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spring.demo.dto.ContactDto;
import spring.demo.util.Utils;

@Repository
public class ContactRepository {

  @Autowired
  private Utils utils;

  public List<ContactDto> getContactsByPersonId(@NonNull Integer id) {
    Connection connection = utils.getConnection();

    List<ContactDto> list = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement("select * from contacts where person_id = ?")) {

      statement.setInt(1, id);

      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        ContactDto contactDto = new ContactDto();
        contactDto.setId(resultSet.getInt("id"));
        contactDto.setEmail(resultSet.getString("email"));
        contactDto.setPhone(resultSet.getString("phone"));
        list.add(contactDto);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
}
