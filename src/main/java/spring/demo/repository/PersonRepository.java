package spring.demo.repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spring.demo.dto.CategoryProductDto;
import spring.demo.dto.ProductDto;
import spring.demo.dto.UserDto;
import spring.demo.util.Utils;

@Repository
public class PersonRepository {
  private int idd;

  @Autowired
  private Utils utils;

  public UserDto checkLogin(UserDto loginDto) {
    Connection connection = utils.getConnection();

    String password = DigestUtils.md5Hex(loginDto.getPassword());
    try (PreparedStatement statement = connection

        .prepareStatement("select * from users where email = '"+loginDto.getEmail()+"' and password = '"+loginDto.getPassword()+"'")) {

      ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()) {
        UserDto personDto = new UserDto();
        personDto.setId(resultSet.getInt("id"));
        personDto.setName(resultSet.getString("name"));
        personDto.setSurname(resultSet.getString("surname"));
        return personDto;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
   return null;
  }

  public List<ProductDto> getPersons(Integer id) {
    Connection connection = utils.getConnection();

    List<ProductDto> list = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement("select * from products where category_id="+id)) {

      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        ProductDto productDto = new ProductDto();
        productDto.setId(resultSet.getInt("id"));
        productDto.setIcon_base64(resultSet.getString("icon_base64"));
        productDto.setName_product(resultSet.getString("name_product"));
         productDto.setBrand_product(resultSet.getString("brand_product"));
         productDto.setDescription(resultSet.getString("description"));
         productDto.setPrice(resultSet.getString("price"));
         productDto.setCount(resultSet.getInt("count"));
        list.add(productDto);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }


  public String add(UserDto userDto) {

    Connection connection = utils.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement("insert into users(email, password, name, surname)  values (?,?,?,?) RETURNING id ")) {
      preparedStatement.setString(1, userDto.getEmail());
      preparedStatement.setString(2, userDto.getPassword());
      preparedStatement.setString(3, userDto.getName());
      preparedStatement.setString(4, userDto.getSurname());
     ResultSet resultSet =  preparedStatement.executeQuery();
        while (resultSet.next()){
          idd = resultSet.getInt("id");

        }
        int a= 121;
        int b = 123456565;
      int random_number1 = a + (int) (Math.random() * b);
      String random = String.valueOf(random_number1);
       PreparedStatement preparedStatement1 = connection.prepareStatement("insert into card(user_id, number_card, cvc, cash) values(?,?,?,?)");
        preparedStatement1.setInt(1,idd);
        preparedStatement1.setString(2,random);
        preparedStatement1.setString(3,"5555");
        preparedStatement1.setInt(4,100000);
        preparedStatement1.executeUpdate();
      return "Регистрация прошла успешна!";

    } catch (SQLException e) {
     e.printStackTrace();
    }

return "Ошибка регистрации!";
  }


  public List<CategoryProductDto> getHome() {
    Connection connection = utils.getConnection();
    List<CategoryProductDto> list = new ArrayList<>();
    try {
      PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from category_products");
     ResultSet resultSet = preparedStatement.executeQuery();
     while (resultSet.next()){
       CategoryProductDto categoryProductDto = new CategoryProductDto();
       categoryProductDto.setId(resultSet.getInt("id"));
       categoryProductDto.setName_category(resultSet.getString("name_category"));
       categoryProductDto.setIcon_base64(resultSet.getString("icon_base64"));
       list.add(categoryProductDto);
     }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

    public ProductDto getInfo(Integer id) {
    Connection connection = utils.getConnection();
      PreparedStatement statement = null;
      try {
        statement = connection.prepareStatement("select * from products where products.id="+id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
          ProductDto productDto = new ProductDto();
          productDto.setId(resultSet.getInt("id"));
          productDto.setIcon_base64(resultSet.getString("icon_base64"));
          productDto.setName_product(resultSet.getString("name_product"));
          productDto.setBrand_product(resultSet.getString("brand_product"));
          productDto.setDescription(resultSet.getString("description"));
          productDto.setPrice(resultSet.getString("price"));
          productDto.setCount(resultSet.getInt("count"));
          return productDto;

        }
      } catch (SQLException e) {
        e.printStackTrace();
      }



    return null;
    }


    public void setAddKor(Integer product_id, Integer user_id) {
        Connection connection = utils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into basket(product_id, user_id) values (?,?)");
            preparedStatement.setInt(1, product_id);
            preparedStatement.setInt(2, user_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }



  public List<ProductDto> getKorzin(Integer id) {
    Connection connection = utils.getConnection();
    List<ProductDto> list = new ArrayList<>();

    try {
    Statement  statement =  connection.createStatement();

      ResultSet resultSet = statement.executeQuery("select distinct p.id,p.category_id,p.icon_base64,p.name_product,p.brand_product,p.description,p.price,p.count from products p left join basket b on p.id = b.product_id where b.user_id = "+id);


      while (resultSet.next()) {
        ProductDto productDto = new ProductDto();
        productDto.setId(resultSet.getInt("id"));
        productDto.setIcon_base64(resultSet.getString("icon_base64"));
        productDto.setName_product(resultSet.getString("name_product"));
        productDto.setBrand_product(resultSet.getString("brand_product"));
        productDto.setDescription(resultSet.getString("description"));
        productDto.setPrice(resultSet.getString("price"));
        productDto.setCount(resultSet.getInt("count"));
        list.add(productDto);



      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public String delete(Integer id) throws SQLException {
    Connection connection = utils.getConnection();
    Statement statement = connection.createStatement();
Integer del =  statement.executeUpdate("DELETE FROM basket WHERE ctid IN (SELECT ctid FROM basket WHERE user_id = '"+id+"' LIMIT 1)");


  return "del"+del;

  }

  public void addPrice() {
    Connection connection = utils.getConnection();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT max(id)  from users");

   while (resultSet.next()) {
     int id = resultSet.getInt("id");
     System.out.println("Person_id" + id);
   }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }


    public String setOplata(Integer id, int price) {
            Connection connection = utils.getConnection();


        try (PreparedStatement preparedStatement = connection.prepareStatement("update card set cash = "+price+" where user_id='"+id+"'and cvc='"+5555+"'")){
          preparedStatement.executeUpdate();

            Statement statement = connection.createStatement();
            Integer del =  statement.executeUpdate("DELETE from basket WHERE user_id = "+id);

            return "Спасибо за покупку";
        } catch (SQLException e) {
         e.printStackTrace();

        }
        return "Вы не верно веели пароль карты";

  }

    public Integer getCash(Integer id) {
      Connection connection = utils.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from card where user_id="+id);
            while (resultSet.next()){
                Integer cash = resultSet.getInt("cash");
                return cash;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void productCount(Integer id, int count) {
      Connection connection = utils.getConnection();
        Statement statement = null;
        Integer x = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from products where id ="+id);
            while (resultSet.next()){
               x = resultSet.getInt("count");
                System.out.println(x);
            }
            Integer a = x - count;
            System.out.println(a);
            PreparedStatement preparedStatement = connection.prepareStatement("update products set count = '"+a+"' where id = "+id);
                preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void productId(Integer id, int count) {
        Connection connection = utils.getConnection();
        Statement statement = null;
        Integer x = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from products where id ="+id);
            while (resultSet.next()){
                x = resultSet.getInt("count");
                System.out.println("minus"+x);
            }
            Integer a = x + count;
            System.out.println("minus"+a);
            PreparedStatement preparedStatement = connection.prepareStatement("update products set count = '"+a+"' where id = "+id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

